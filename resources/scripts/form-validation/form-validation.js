function validateForm(){
    var errorFields = [];
    var dateField = document.getElementById("datetime");
    var reservationFields = [document.getElementById("amount-adults"), document.getElementById("amount-children"), document.getElementById("amount-infants")];
    var validDate = validateDate();
    var validVisitors = validateVisitors();

    //Start by running one function to check if the date is valid, and another one to see if the amount of visitors is valid.
    var email = document.getElementById("email").value;
    var phoneNumber = document.getElementById("phonenumber").value;
    var names = [document.getElementById("first-name").value, document.getElementById("last-name").value];
    var comment = document.getElementById("comment").value;
    //Getting values from the form and storing them here.
    if(validDate&&validVisitors){
        console.log("Checks out");
        alert("Thank you, " + names[0] + ", for your reservation! A confirmation email has been sent to "+ email + ", as well as per sms to " +phoneNumber);
        return true;
        //The form will validate (I.E, validateForm will return true) if the date and visitors are valid (they both return true);
    }else{
        highlightErrors(errorFields);
        return false;
        //The form will not walidate if either validDate or validVisitors returns false (or fails to validate, if you will).
        //highlightErrors will also highlight the erronomous fields for the user.
    }
    function validateDate(){
        //This functions purpose is to check if the DATE AND TIME are valid. This means that the date and time has to be either right now or in the future.
        var today = new Date();
        var reservedDate = convertToDate(dateField.value);
        //I use a third party tool called rome to get the date and time from the user. Since I want to compare it with todays date, stored as a date type variable with the name "today", the datetime string
        //from rome needs to be converted into a date, so that it can be compared with "today". convertToDate takes care of this.

        if(reservedDate>=today){
            var open = checkOpeningTime(reservedDate);
            //if the desired date and time of reservation (reserved date) is after or equal to today, the opening and closing times have to be checked against the
            //desired reservation time. checkOpeningTime takes the reserved date, checks whether it is within the opening hours of the shop, and returns true or false based on the result.
            return open;
            //validDate returns the result of the openinghours check.
        }else{
            alert("The specified date and time is in the past. Please book a time in the present or future.");
            errorFields.push(dateField);
            return false;
            //if the reservedDate is less than today, I.E the desired date is in the past, validateDate returns false and the form validation will fail.
            //It will also add it to a list of failed fields.
        }
    }

    function validateVisitors(){

        var reservations = [reservationFields[0].value, reservationFields[1].value, reservationFields[2].value];
        //Get the values of the fields with the amount of people to reserve for.
        var totalReservations = 0;
        //Amount of total reservations declared and initialized at 0.
        for(var i = 0; i<reservations.length; i++){
            if(reservations[i]==""){
                reservations[i] = 0;
                //If statements that "catches" empty fields, and puts a 0 in them.
            }
            reservations[i] = parseInt(reservations[i]);
            //Takes the current reservation value, and parses it into an integer, as i want to add together the numerical values, and not strings.
            totalReservations+=reservations[i];
            //amount of total visitors too book for is increased by the current amount.
        }

        if(totalReservations<=0){
            alert("No guests have been specified! Please make a booking for at least one person.");
            for(var i  = 0; i < reservationFields.length; i++){
                errorFields.push(reservationFields[i]);
            }
            return false;
            //if the total amount of guests reserved for is zero or less, the user is alerted, and the validation fails.
        }
        else if(reservations[0]==0&&reservations[2]>0){
            errorFields.push(reservationFields[0]);
            alert("Infants may not go without parental supervision! Infants must be accompanied by at least one adult!");
            return false;
            //the same happens if the reservation has more than 0 infants, but no adults to accompany them.
            //In both cases, the relevant fields are added to the list of erronomous fields.
        }else{
            console.log(totalReservations);
            console.log("Reserved for: " + reservations[0] + " Adults, " + reservations[1] +" Children, and " + reservations[2] + " Infants. ");
            return true;
            //if the reservation has an acceptable number of people with no single infants, this part of the validation returns true.
        }
    }

    function convertToDate(reservedDate){
        var times = reservedDate.split(/[ -]/);
        var hoursMinutes = times[3].split(":");
        var result = new Date(times[0], times[1]-1, times[2], hoursMinutes[0], hoursMinutes[1]);
        return result;
        //this function takes the reserved date, and splits it into an array of values, representing year, month, date, hours, and minutes.
        //The split is done using a regular experssion pattern. It will split by open spaces and dashes.
    }

    function checkOpeningTime(reservedDate){
        var weekdays =["Sunday", "Monday", "Tuesday", "Wednesday","Thursday", "Friday", "Saturday"];
        var closingHours = ["21:30", "20:00", "20:00", "20:00","20:00", "21:30", "22:00"];
        var openingHour = "10:00";
        //Above we can see the databank of days and opening hours.
        var hoursAndMinutesToClose = closingHours[reservedDate.getDay()].split(":");
        //here the closing hour is split into hours and minutes, after finding the right day by using getDay on the reserved date.
        var openingHourAndMinute = openingHour.split(":");    
        //and here the opening hour is split 
        var closingTime = new Date(2000, 00, 01, hoursAndMinutesToClose[0], hoursAndMinutesToClose[1]);

        var toCheck = new Date(2000, 00, 01, reservedDate.getHours(), reservedDate.getMinutes());
        //The above two variables are used to check if the reservation is done within opening hours. They have the same day...

        if(toCheck<closingTime&&toCheck.getHours()>=openingHourAndMinute[0]){
            //So that when they are compared, only the hours and minutes are directly compared.
            console.log("This booking is during opening hours! We can accept!");
            return true;
            //This function will also return true if the reservation is valid. When both the people validation and time validation are good, the validateForm function will return true to the form, and it
            //will succesfully validate!
        }else{
            alert("We can only accept bookings during opening hours.\nOn your chosen day, " + weekdays[reservedDate.getDay()] + ", we are open from " + openingHour + " to " + closingHours[reservedDate.getDay()]);
            errorFields.push(dateField);
            return false;
        }

    }
    function highlightErrors(errorFields){
        for(var i = 0; i< errorFields.length; i++){
            errorFields[i].classList.add("error");
            errorFields[i].addEventListener("click", removeErrors);
            //Adds an error class (stylid in css) to all erronomous fields, as well as an aventlistener to remove the style on click.
        }
    }
    function removeErrors(){
        if(this.classList.contains("amount-people")){
            for(var i = 0; i < reservationFields.length; i++){
                console.log("removing");
                reservationFields[i].classList.remove("error");
                reservationFields[i].removeEventListener("click", removeErrors);
                //if the field clicked is one of the fields for the amount of people you wish to make a booking for, all the people fields get the error removed.
            }
        }else{
            this.classList.remove("error");
            this.removeEventListener("click", removeErrors);
            //If not, it will just go through the list. In this case the list can only contain the date field, so the date field loses its error style if clicked after an error.
        }
    }
}