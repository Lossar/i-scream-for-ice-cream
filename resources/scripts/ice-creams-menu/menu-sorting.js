var iceMenus = document.getElementsByClassName("ice-cream-menu");
var buttons = document.getElementsByClassName("filter-button");
var sortingButtons = document.getElementsByClassName("sorting-button");
//Get all the ice cream menus and buttons from the document...

sortingButtons[0].addEventListener("click", sortByPrice, true);
sortingButtons[1].addEventListener("click", sortByName, true);
sortingButtons[2].addEventListener("click", sortByDescription, true);
//... and attatch listeners to the buttons. I could also have used on click, but I ran into problems with passing arguments from
//the html into the javascript, so I figure it is best for the script to "listen" for the clicks.

for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", filterByName, true);
    //add filterByName function to all filter buttons... filterbyname uses the value of the button, and compares it to the id's of all the menus...
}

function filterByName() {
    for (var i = 0; i < iceMenus.length; i++) {
        //Go through all the iceMenus in the html...
        if (this.value == "all") {
            //if the all button is pressed (the pressed button has a value equal to all)...
            iceMenus[i].classList.remove("hidden");
            //Remove the hidden class(if any). The hidden class has one rule: display: none. This hides the element, and makes it not occupy space, giving the illusion that it has been
            //removed.
        } else if (iceMenus[i].id != this.value) {
            //if the value is not all, and the value of the button is not the one of the current iceMenu...
            iceMenus[i].classList.add("hidden");
            //Hide the icemenu, by giving it the hidden class.
        } else {
            iceMenus[i].classList.remove("hidden");
            //If the value is not all, and the value is not different from the icemenu, we are at the correct one, and the hidden class is removed.
        }
    }
}

function sortByPrice() {
    for (var z = 0; z < categories.length; z++) {
        //go through all categories...
        listTosort = iceCreams[categories[z]];
        //get the array to sort from the database.
        htmlList = document.getElementById("menu" + [z]);
        //get the menu item that corresponds with the current category.
        for (var i = 0; i < listTosort.length; i++) {
            for (var x = 0; x < listTosort.length; x++) {
                if (listTosort[i].price < listTosort[x].price) {
                    //if the current price is lesser than the price checked against, swap places.
                    var temp = listTosort[i];
                    //store current element in temporary variable.
                    listTosort[i] = listTosort[x];
                    //make currentvariable the other variable.
                    listTosort[x] = temp;
                    //and finally make the other value equal to the temporary variable, which has the value of the current variable BEFORE it got changed.
                }
            }
        }
        //Sort the element, by swapping the positions of the elements in each array in the database around based on the prices.
        rewriteElements(htmlList, listTosort);
        //sortyByPrice, sortByDescription, and sortByName handle the sorting, while rewriteElements handles the alterations of the html.
        //They pass the html list (which is the parent div for each menu-item in each category), and the freshly re-ordered database array for whatever
        //category of dessert they have just sorted.
    }
}

function sortByName() {
    for (var z = 0; z < categories.length; z++) {
        //again, go through all categories to sort them.
        listTosort = iceCreams[categories[z]];
        htmlList = document.getElementById("menu" + [z]);
        listTosort.sort(function (a, b) {
            var nameOfArgA = a.name.toLowerCase();
            var nameOfVarB = b.name.toLowerCase();
            if (nameOfArgA < nameOfVarB)
                return -1;
            if (nameOfArgA > nameOfVarB) {
                return 1;
            }
            return 0;
        });
        //This funtion sorts by using a sorting function that compares a (name of one ice cream to compare) and b (name of an ice cream to compare to) and returns a value based on the results. 
        //This sorts the list by using the reutrned values. 
        rewriteElements(htmlList, listTosort);
    }
}

function sortByDescription() {
    for (var z = 0; z < categories.length; z++) {
        listTosort = iceCreams[categories[z]];
        htmlList = document.getElementById("menu" + [z]);
        console.log(listTosort);
        listTosort.sort(function (a, b) {
            var descriptionOfArgA = a.description.toLowerCase();
            var descriptionOfArgB = b.description.toLowerCase();
            if (descriptionOfArgA < descriptionOfArgB)
                return -1;
            if (descriptionOfArgA > descriptionOfArgB) {
                return 1;
            }
            return 0;
        });
        //basically the same as the name sorting, but using the descriptions instead.
        rewriteElements(htmlList, listTosort);
    }
}

function rewriteElements(htmlList, databaseList) {
    /*This function handles the actual rewrites of the html elements*/
    htmlItems = htmlList.getElementsByTagName("div");
    /*Gets the divs of the set of html items given as an argument, forexample all the divs inside the larger div for mousse menu-items.*/
    for (var i = 0; i < htmlItems.length; i++) {
        //for every div....
        var header = htmlItems[i].getElementsByTagName("h3");
        var spans = htmlItems[i].getElementsByTagName("span");
        //we get the header and spans...

        spans[0].innerHTML = databaseList[i].description;
        spans[1].innerHTML = databaseList[i].price + " NOK";
        header[0].innerHTML = databaseList[i].name;
        //and re-write them in the new, sorted order!
    }
}